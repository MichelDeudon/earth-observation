def test_boat_model(app_prefix="./"):
    ''' Test pytorch model to detect and count objects in sentinel-2 imagery'''
    
    import os
    import sys
    sys.path.insert(0,os.path.dirname(app_prefix))
    sys.path.insert(0,os.path.dirname(os.path.join(app_prefix,'src/')))

    import torch
    from model import BoatNet

    batch_size, input_dim, H, W = 8, 1, 80, 80
    x = torch.zeros((batch_size, input_dim, H, W))
    y = torch.ones((batch_size, 1))

    hidden_dim, pool_size = 32, 2
    model = BoatNet(input_dim=input_dim, hidden_dim=hidden_dim, kernel_size=3, pool_size=pool_size, pad=True, device='cpu', version=1.0)
    density_map, p_max, p_med, y_hat = model(x)
    assert density_map.shape == (batch_size, 1, H//(pool_size**3), W//(pool_size**3)) # density map
    assert p_max.shape == y.shape # proba boat presence
    assert y_hat.shape == y.shape # boat counts (expectation)

    metrics = model.get_loss(x, y, ld=0.8)
    for metric in ['loss', 'clf_error', 'reg_error', 'accuracy', 'precision', 'recall', 'f1']:
        assert metric in metrics.keys()

    heatmaps, counts = model.chip_and_count(x)
    assert len(heatmaps) == batch_size and heatmaps[0].shape == (H//(pool_size**3), W//(pool_size**3))
    assert len(counts) == batch_size and isinstance(counts[0], float)

def test_checkpoint(app_prefix="./", checkpoint_dir="./models/weights/boatnet/"):
    ''' Load checkpoint for pretrained model'''

    import os
    import sys
    sys.path.insert(0,os.path.dirname(app_prefix))
    sys.path.insert(0,os.path.dirname(os.path.join(app_prefix,'src/')))

    # Load pretrained model
    from model import load_model
    model = load_model(checkpoint_dir=checkpoint_dir, version=1.0, input_dim=1, hidden_dim=32, kernel_size=5, pool_size=2)
