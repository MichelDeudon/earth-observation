datetime
geopandas
jupyter
matplotlib
numpy
openeo
pandas
plotly
pytest
rasterio
scikit-image
shapely
tqdm
-f https://download.pytorch.org/whl/torch_stable.html 
torch 
-f https://download.pytorch.org/whl/torch_stable.html
torchvision